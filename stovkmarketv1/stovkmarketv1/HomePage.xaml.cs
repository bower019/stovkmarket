﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using stovkmarketv1.Models;

namespace stovkmarketv1
{
	
	public partial class HomePage : ContentPage
	{
        public static StockData SData;

        public HomePage ()
		{
			InitializeComponent ();
		}

        async void HandleStockData(string symbol)
        {
            // test
            var client = new HttpClient();
            var stockApiAddress = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=IL8AS15CCS4OUJOD";
            var uri = new Uri(stockApiAddress);

            SData = new StockData();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                SData = JsonConvert.DeserializeObject<StockData>(jsonContent);

            }
            foreach (var item in SData.TimeSeriesDaily)
            {
                item.Value.Date= item.Key;
            }
          
            StockView.ItemsSource = SData.TimeSeriesDaily;
            if (SData.TimeSeriesDaily != null)
            {
                Searchstock.Text = symbol;
                Highest.Text = SData.TimeSeriesDaily.Values.Max(a => a.High);
                Lowest.Text = SData.TimeSeriesDaily.Values.Min(a => a.Low);

            }
            Searchstock.Text = symbol;
        }

        void Handle_SearchButtonPressed(object sender, EventArgs args)
        {
            
        }

        void OnButtonClicked(object sender, EventArgs args)
        {
            HandleStockData(Searchstock.Text);
        }

    }
}